class Calculations {
    static Point2D positionGeometricCenter(Point2D[] points) {
        double X = 0.0
        double Y = 0.0
        int size = points.size()
        for (int i = 0; i < size; i++) {
            X += points[i].getX()
            Y += points[i].getY()
        }
        return new Point2D(X / size, Y / size)
    }

    static Point2D positionCenterOfMass(MaterialPoint2D[] materialPoint) {
        double x = 0.0
        double y = 0.0
        double mass = 0.0
        for(int i = 0; i < materialPoint.length; i++) {
            x += materialPoint[i].getX() * materialPoint[i].getMass()
            y += materialPoint[i].getY() * materialPoint[i].getMass()
            mass += materialPoint[i].getMass()
        }

        return new MaterialPoint2D(x / mass, y / mass, mass)
    }
}
